cis-config
==========

Repo that will contain config files that are compliant with CIS security benchmarks. Will be looking into adding scripts
that will automagically apply configs to host(s).

First phase of the change will be to add switches to integration script to distinguish between level 1 and level 2 settings. 

Planning on breaking script down into separate functions for the different sections. 

Pull requests to come shortly.
