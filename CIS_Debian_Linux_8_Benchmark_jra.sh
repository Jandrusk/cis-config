#!/bin/bash
PROFILE=${1:-Level 1}

function secure-partitions {
    echo \*\*\*\* Executing Level 1 profile remediation

  # Create Separate Partition for /tmp
  echo
  echo \*\*\*\* Create\ Separate\ Partition\ for\ /tmp
  echo Create\ Separate\ Partition\ for\ /tmp not configured.

  # Set nodev option for /tmp Partition
  echo
  echo \*\*\*\* Set\ nodev\ option\ for\ /tmp\ Partition
  egrep -q "^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/tmp\2nodev\3\4/" /etc/fstab

  # Set nosuid option for /tmp Partition
  echo
  echo \*\*\*\* Set\ nosuid\ option\ for\ /tmp\ Partition
  egrep -q "^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/tmp\2nosuid\3\4/" /etc/fstab

  # Set noexec option for /tmp Partition
  echo
  echo \*\*\*\* Set\ noexec\ option\ for\ /tmp\ Partition
  egrep -q "^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/tmp(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/tmp\2noexec\3\4/" /etc/fstab

  # Create Separate Partition for /var
  echo
  echo \*\*\*\* Create\ Separate\ Partition\ for\ /var
  echo Create\ Separate\ Partition\ for\ /var not configured.

  # Bind Mount the /var/tmp directory to /tmp
  echo
  echo \*\*\*\* Bind\ Mount\ the\ /var/tmp\ directory\ to\ /tmp
  echo Bind\ Mount\ the\ /var/tmp\ directory\ to\ /tmp not configured.

  # Create Separate Partition for /var/log
  echo
  echo \*\*\*\* Create\ Separate\ Partition\ for\ /var/log
  echo Create\ Separate\ Partition\ for\ /var/log not configured.

  # Create Separate Partition for /var/log/audit
  echo
  echo \*\*\*\* Create\ Separate\ Partition\ for\ /var/log/audit
  echo Create\ Separate\ Partition\ for\ /var/log/audit not configured.

  # Create Separate Partition for /home
  echo
  echo \*\*\*\* Create\ Separate\ Partition\ for\ /home
  echo Create\ Separate\ Partition\ for\ /home not configured.

  # Add nodev Option to /home
  echo
  echo \*\*\*\* Add\ nodev\ Option\ to\ /home
  egrep -q "^(\s*\S+\s+)/home(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/home(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/home\2nodev\3\4/" /etc/fstab

  # Add nodev Option to /run/shm Partition
  echo
  echo \*\*\*\* Add\ nodev\ Option\ to\ /run/shm\ Partition
  egrep -q "^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/run/shm\2nodev\3\4/" /etc/fstab

  # Add nosuid Option to /run/shm Partition
  echo
  echo \*\*\*\* Add\ nosuid\ Option\ to\ /run/shm\ Partition
  egrep -q "^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/run/shm\2nosuid\3\4/" /etc/fstab

  # Add noexec Option to /run/shm Partition
  echo
  echo \*\*\*\* Add\ noexec\ Option\ to\ /run/shm\ Partition
  egrep -q "^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$" /etc/fstab && sed -ri "s/^(\s*\S+\s+)/run/shm(\s+\S+\s+\S+)(\s+\S+\s+\S+)(\s*#.*)?\s*$/\1/run/shm\2noexec\3\4/" /etc/fstab

 }
