#!/bin/bash

  # Set Permissions on bootloader config
  echo
  echo \*\*\*\* Set\ Permissions\ on\ bootloader\ config
  chmod g-r-w-x,o-r-w-x /boot/grub/grub.cfg

  # Set Boot Loader Password
  echo
  echo \*\*\*\* Set\ Boot\ Loader\ Password
  echo Set\ Boot\ Loader\ Password not configured.

  # Require Authentication for Single-User Mode
  echo
  echo \*\*\*\* Require\ Authentication\ for\ Single-User\ Mode
  echo Require\ Authentication\ for\ Single-User\ Mode not configured.

  
