#!/bin/bash

# Ensure NIS is not installed
echo
echo \*\*\*\* Ensure\ NIS\ is\ not\ installed
dpkg -s nis && apt-get -y purge nis

# Ensure rsh server is not enabled
echo
echo \*\*\*\* Ensure\ rsh\ server\ is\ not\ enabled
sed -ri "s/^shell/#shell/" /etc/inetd.conf
sed -ri "s/^login/#login/" /etc/inetd.conf
sed -ri "s/^exec/#exec/" /etc/inetd.conf

# Ensure rsh client is not installed
echo
echo \*\*\*\* Ensure\ rsh\ client\ is\ not\ installed
dpkg -s rsh-client && apt-get -y remove rsh-client
dpkg -s rsh-redone-client && apt-get -y remove rsh-redone-client

# Ensure talk server is not enabled
echo
echo \*\*\*\* Ensure\ talk\ server\ is\ not\ enabled
sed -ri "s/^talk/#talk/" /etc/inetd.conf
sed -ri "s/^ntalk/#ntalk/" /etc/inetd.conf

# Ensure talk client is not installed
echo
echo \*\*\*\* Ensure\ talk\ client\ is\ not\ installed
dpkg -s talk && apt-get -y remove talk

# Ensure telnet server is not enabled
echo
echo \*\*\*\* Ensure\ telnet\ server\ is\ not\ enabled
sed -ri "s/^telnet/#telnet/" /etc/inetd.conf

# Ensure tftp-server is not enabled
echo
echo \*\*\*\* Ensure\ tftp-server\ is\ not\ enabled
sed -ri "s/^tftp/#tftp/" /etc/inetd.conf

# Ensure xinetd is not enabled
echo
echo \*\*\*\* Ensure\ xinetd\ is\ not\ enabled
update-rc.d xinetd disable

# Ensure chargen is not enabled
echo
echo \*\*\*\* Ensure\ chargen\ is\ not\ enabled
sed -ri "s/^chargen/#chargen/" /etc/inetd.conf

# Ensure daytime is not enabled
echo
echo \*\*\*\* Ensure\ daytime\ is\ not\ enabled
sed -ri "s/^daytime/#daytime/" /etc/inetd.conf

# Ensure echo is not enabled
echo
echo \*\*\*\* Ensure\ echo\ is\ not\ enabled
sed -ri "s/^echo/#echo/" /etc/inetd.conf

# Ensure discard is not enabled
echo
echo \*\*\*\* Ensure\ discard\ is\ not\ enabled
sed -ri "s/^discard/#discard/" /etc/inetd.conf

# Ensure time is not enabled
echo
echo \*\*\*\* Ensure\ time\ is\ not\ enabled
sed -ri "s/^time/#time/" /etc/inetd.conf

# Ensure the X Window system is not installed
echo
echo \*\*\*\* Ensure\ the\ X\ Window\ system\ is\ not\ installed
apt-get -y purge xserver-xorg-core*

# Ensure Avahi Server is not enabled
echo
echo \*\*\*\* Ensure\ Avahi\ Server\ is\ not\ enabled
systemctl disable avahi-daemon

# Ensure DHCP Server is not enabled
echo
echo \*\*\*\* Ensure\ DHCP\ Server\ is\ not\ enabled
update-rc.d isc-dhcp-server disable

# Configure Network Time Protocol (NTP)
echo
echo \*\*\*\* Configure\ Network\ Time\ Protocol\ \(NTP\)
dpkg -s ntp || apt-get -y install ntp
egrep -q "^\s*restrict(\s+-4)?\s+default(\s+\S+)*(\s*#.*)?\s*$" /etc/ntp.conf && sed -ri "s/^(\s*)restrict(\s+-4)?\s+default(\s+[^[:space:]#]+)*(\s+#.*)?\s*$/\1restrict\2 default kod nomodify notrap nopeer noquery\4/" /etc/ntp.conf || echo "restrict default kod nomodify notrap nopeer noquery" >> /etc/ntp.conf 
egrep -q "^\s*restrict\s+-6\s+default(\s+\S+)*(\s*#.*)?\s*$" /etc/ntp.conf && sed -ri "s/^(\s*)restrict\s+-6\s+default(\s+[^[:space:]#]+)*(\s+#.*)?\s*$/\1restrict -6 default kod nomodify notrap nopeer noquery\3/" /etc/ntp.conf || echo "restrict -6 default kod nomodify notrap nopeer noquery" >> /etc/ntp.conf 
egrep -q "^(\s*)OPTIONS\s*=\s*\"(([^\"]+)?-u\s[^[:space:]\"]+([^\"]+)?|([^\"]+))\"(\s*#.*)?\s*$" /etc/sysconfig/ntpd && sed -ri '/^(\s*)OPTIONS\s*=\s*\"([^\"]*)\"(\s*#.*)?\s*$/ {/^(\s*)OPTIONS\s*=\s*\"[^\"]*-u\s+\S+[^\"]*\"(\s*#.*)?\s*$/! s/^(\s*)OPTIONS\s*=\s*\"([^\"]*)\"(\s*#.*)?\s*$/\1OPTIONS=\"\2 -u ntp:ntp\"\3/ }' /etc/sysconfig/ntpd && sed -ri "s/^(\s*)OPTIONS\s*=\s*\"([^\"]+\s+)?-u\s[^[:space:]\"]+(\s+[^\"]+)?\"(\s*#.*)?\s*$/\1OPTIONS=\"\2\-u ntp:ntp\3\"\4/" /etc/sysconfig/ntpd || echo "OPTIONS=\"-u ntp:ntp\"" >> /etc/sysconfig/ntpd
echo Configure\ Network\ Time\ Protocol\ \(NTP\) - server not configured.

# Ensure LDAP is not enabled
echo
echo \*\*\*\* Ensure\ LDAP\ is\ not\ enabled
dpkg -s slapd && apt-get -y purge slapd

# Configure Mail Transfer Agent for Local-Only Mode
echo
echo \*\*\*\* Configure\ Mail\ Transfer\ Agent\ for\ Local-Only\ Mode
echo Configure\ Mail\ Transfer\ Agent\ for\ Local-Only\ Mode Linux custom object not configured.

# Ensure rsync service is not enabled
echo
echo \*\*\*\* Ensure\ rsync\ service\ is\ not\ enabled
dpkg -s rsync && sed -ri "s/^(\s*RSYNC_ENABLE\s*=\s*)\S+(\s*)/\1false\2/" /etc/default/rsync


